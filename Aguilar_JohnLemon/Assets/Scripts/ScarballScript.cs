﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScarballScript : MonoBehaviour
{
    // Calls in the Scarball's collider 
    SphereCollider m_sphereCollider;
    Rigidbody m_rigidbody;
    private Material mymat;
    private PlayerMovement playerMovement;
    public AudioSource catchGhost;






    void Start()
    {
        m_sphereCollider = GetComponent<SphereCollider>();
        
        playerMovement = FindObjectOfType<PlayerMovement>();
        mymat = GetComponent<Renderer>().material;
        

    }



   

    void OnTriggerEnter(Collider other)
    {


        // Stops the balls from going through the floor or when hits a solid surface
        // Despawns ball after 5 sec to give player chance to pick up 
        if (other.gameObject.CompareTag("Floor"))
        {
            gameObject.GetComponent<SphereCollider>().isTrigger = false;
            Destroy(gameObject, 3f);
        } 

        // Capture the Ghost, still needs a count to tell player how many caught  

        if (!other.isTrigger)
        {
            if (other.gameObject.CompareTag("Ghost"))
            {
                
                Destroy(other.gameObject);
                

                mymat.SetColor("_EmissionColor", Color.red);
                mymat.EnableKeyword("_EMISSION");
               
                playerMovement.ghostCount = playerMovement.ghostCount + 1;
                playerMovement.SetGhostCountText();
                catchGhost.Play();



            }

        }
        

    }
   
}
