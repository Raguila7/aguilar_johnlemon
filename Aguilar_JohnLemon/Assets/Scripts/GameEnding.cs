﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEnding : MonoBehaviour
{
    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    public PlayerMovement player;
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public AudioSource exitAudio;
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public GameObject flashScreen;
    public AudioSource caughtAudio;
    
    public AudioSource musicAudio;


    bool m_IsPlayerAtExit;
    bool m_IsPlayerCaught;
    float m_Timer;
    bool m_HasAudioPlayed;


    void Start()
    {
        musicAudio.Play();
       

        flashScreen.SetActive(true);
    }

    //Function that will allow player to trigger an event when walking through the box
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player.gameObject && player.ghostCount >= 11)
        {
            m_IsPlayerAtExit = true;
        }
    }
    public void CaughtPlayer()
    {
        m_IsPlayerCaught = true;
    }
    //Only gets called in the previous Ontrigger enter function is called

    void Update()
    {
        if (m_IsPlayerAtExit)
        {
            EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio);
        }
        // End the game if the player is caught or seen
        else if (m_IsPlayerCaught)
        {
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
        }
    }

    void EndLevel (CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
    {
        musicAudio.Stop();
        if (!m_HasAudioPlayed)
        {
            audioSource.Play();
            m_HasAudioPlayed = true;
        }

        m_Timer += Time.deltaTime;

        //Gets the Alpha and updated from the 0 and will go up to 1 
        imageCanvasGroup.alpha = m_Timer / fadeDuration;
        // Stop the previous code from continuing but allows images to be displayed 
        if (m_Timer > fadeDuration + displayImageDuration)
            if (doRestart)
            {
                //Restart Level. Scene called is 0 because its the only scene saved 
                SceneManager.LoadScene(0);
            }
            else
            {
                Application.Quit();
            }
    }

}
