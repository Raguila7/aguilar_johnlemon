﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;
    public TextMeshProUGUI ballcountText;
    public TextMeshProUGUI ghostcountText;

    public Slider chargeSlider;
    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    public AudioSource throwball;
    public AudioSource pickupFlash;
    public AudioSource pickupBall;
    public AudioSource flashAudio;



    // Creating a varible to call in the vector3 
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;

    // Projectile varibles 
    public GameObject scarballPrefab;
    public Transform shotSpawnshot;
    public Transform shotSpawnLob;
    public Image flashscreen;

    public int ghostCount;
    private int ballCount;
    private int flashCharges;
    private int flashUIcharge;
    public bool invulnerable;
    public bool isFlashOn;



    public float shotSpeed = 10f;

    // Calls in all ghosts waypoints
    public WaypointPatrol[] allPatrols;


    // Start is called before the first frame update
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
        ballCount = 0;
        SetSlider(); 
        flashCharges = 0;
        SetBallCountText();
        ghostCount = 0;
        SetGhostCountText();
        flashscreen.enabled = false;
        isFlashOn = false;

        allPatrols = FindObjectsOfType<WaypointPatrol>();
    }

    void Update()
    {
        // Normal Shot
        if (Input.GetKeyDown(KeyCode.Space) && ballCount > 0)
        {
            GameObject scarball = Instantiate(scarballPrefab, shotSpawnshot.transform.position, scarballPrefab.transform.rotation);

            Rigidbody scarballRB = scarball.GetComponent<Rigidbody>();
            scarballRB.velocity = shotSpawnshot.forward * shotSpeed;

            ballCount = ballCount - 1;

            AudioPlaying(throwball);

           

            SetBallCountText();
            
        }

        // Lob shot
        else if (Input.GetKeyDown(KeyCode.E) && ballCount > 0)
        {
            GameObject scarball = Instantiate(scarballPrefab, shotSpawnLob.transform.position, scarballPrefab.transform.rotation);

            Rigidbody scarballRB = scarball.GetComponent<Rigidbody>();
            scarballRB.velocity = shotSpawnLob.forward * shotSpeed;

            ballCount = ballCount - 1;
            AudioPlaying(throwball);


            SetBallCountText();
        }

        // Camera Flash Abiilty 

        if (Input.GetKeyDown(KeyCode.Q) && flashCharges > 0)
        {
            AudioPlaying(flashAudio);
            FlashGhosts();
            SetSlider();

        }
    }
    // Stops ghosts
    void FlashGhosts()
    {
        foreach (WaypointPatrol patrol in allPatrols)
        {
            if (patrol != null)
            {
                patrol.navMeshAgent.isStopped = true;
                // Makes ghost not able to see player 
                invulnerable = true;
                print("he cant see " + invulnerable);

                StartCoroutine(Flash());



            }
        }

        CancelInvoke("EndFlash");
        Invoke("EndFlash", 3.5f);
        flashCharges = flashCharges - 1;
    }


    void EndFlash()
    {
        foreach (WaypointPatrol patrol in allPatrols)
        {
            if (patrol != null)
            {
                patrol.navMeshAgent.isStopped = false;
                invulnerable = false;
                print("he can see " + invulnerable);
            }
        }
    }



    public IEnumerator Flash()
    {

        flashscreen.enabled = true;
        isFlashOn = true;
        yield return new WaitForSeconds(0.05f);
        flashscreen.enabled = false;
        isFlashOn = false;
        yield return new WaitForSeconds(0.2f);
        flashscreen.enabled = true;
        isFlashOn = true;
        yield return new WaitForSeconds(0.15f);
        flashscreen.enabled = false;
        isFlashOn = false;



    }


    void SetSlider()
    {
        chargeSlider.value = flashCharges;
    }


    public void SetGhostCountText()
    {
        
        ghostcountText.text = ghostCount.ToString() + "/11";

    }

    void SetBallCountText()
    {
        ballcountText.text = "x" + ballCount.ToString();
    }

    // Update is called once per frame
    void FixedUpdate ()
    {

        //Getting the Input we need to move it a horizontal and vertical movement
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);

        // Allow the player to walk in a horizontal way or vertical or diagonally

        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);

        if (isWalking)
        {
            // Give the player footstep audio as long as they continue to move
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        //Given the player info where to look 
        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        // Where the player actaully looks 
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }

    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }

    private void OnTriggerEnter(Collider other)
    {

        // Projectile Pickup 
        if (other.gameObject.CompareTag("PickUp"))
        {
            
            other.gameObject.SetActive(false);

            ballCount = ballCount + 5;
            AudioPlaying(pickupBall);


            SetBallCountText();

        }

        //Camera Pickup 
        if (other.gameObject.CompareTag("Flash"))
        {
            
            other.gameObject.SetActive(false);


            flashCharges = flashCharges + 1;
            AudioPlaying(pickupFlash);
            print(flashCharges + " flash");
            SetSlider();

        }

    }
    // Allows player to pick up missed shots before they despawn
    private void OnCollisionEnter(Collision col)
    {
        
            if (col.gameObject.name == "SCARball(Clone)")
            {
                
                Destroy(col.gameObject);
                ballCount = ballCount + 1;
            AudioPlaying(pickupBall);

            SetBallCountText();
        }
        
    }

    void AudioPlaying (AudioSource audioSource)
    {
        audioSource.Play();
    }
}
