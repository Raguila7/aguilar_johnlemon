﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Observer : MonoBehaviour
{
    //Calls Transform instead of Game Object because makes it easier to access J.Lemons position and line of sight
    public Transform player;

    bool m_IsPlayerInRange;

    public GameEnding gameEnding;
   
    // While the player is in sight 
    void OnTriggerEnter(Collider other)
    {
        if (other.transform == player)
        {
            m_IsPlayerInRange = true;
        }
    }
    //While the player exits or remains out of sight
    void OnTriggerExit(Collider other)
    {
        if (other.transform == player)
        {
            m_IsPlayerInRange = false;
        }
    }
    void Update()
    {
        //What happens when you enter in range
        if (m_IsPlayerInRange)
        {
            //This part is need in order to make sure the light of sight is clear or that nothing gets in the way of blocking the line of sight to the player
            Vector3 direction = player.position - transform.position + Vector3.up;
            Ray ray = new Ray(transform.position, direction);
            RaycastHit raycastHit;
            //When the raycast hits the player
            if (Physics.Raycast(ray, out raycastHit))
            {
                if (raycastHit.collider.transform == player)
                {
                    PlayerMovement playerMovement = player.GetComponent<PlayerMovement>();

                    if (!playerMovement.invulnerable)
                    gameEnding.CaughtPlayer();
                }
            }
        }
    }
}